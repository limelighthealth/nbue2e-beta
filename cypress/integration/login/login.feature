@automation
Feature: Login to Application
  As a valid user
  I want to log in to the Application

  @blocker
  Scenario Outline: Valid Login
    Given I login as a "<user>"
    Then I should be able two access create groups, groups proposals, and tools
    Examples:
      | user   |
      | broker |
