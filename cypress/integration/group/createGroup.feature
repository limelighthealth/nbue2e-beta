@automation @createGroup
Feature: Create a Group
  As a valid user
  I want to be able to create a group
  So that I can record my group's data in my application

  @blocker
  Scenario Outline: Create a Group
    Given I login as a "<user>"
    And I create a group via QSL with the following data
      | group_file_name | owner_name       |
      | qsl group       | MiddleOrg Broker |
    When I navigate to "Group Home" page
    Examples:
      | user        |
      | underwriter |
