/**
 * Committing this file to illustrate how this pattern won't work in Cypress.
 * We cannot have functions that return promises anymore - we have to use
 * the .then() syntax in our step_definitions
 */
import * as pathUtils from 'path';
import promisify from 'cypress-promise';

export async function getJsonFromFile(filePath: any): Promise<object> {
  filePath = filePath.split(' ').join('_') + '.json';
  filePath = pathUtils.normalize('cypress/support/test_data/' + filePath);

  //I tried out the cypress-promise library to see if it would work. It doesn't.
  const jsonObject =  await promisify(cy.readFile(filePath).then(obj => { return obj}));
  return jsonObject;
}

export function findJsonValue(obj, prop) {
  prop = prop.split('.');
  for (let i = 0; i < prop.length; i++) {
    if (typeof obj[prop[i]] === 'undefined') {
      throw Error('Json value not found: ' + prop);
    }
    obj = obj[prop[i]];
  }
  return obj;
}

export function jsonToMap(jsonObj): Map<string, object> {
  const map: Map<string, object> = new Map();
  Object.keys(jsonObj).forEach(key => {
    map.set(key, jsonObj[key]);
  });
  return map;
}
