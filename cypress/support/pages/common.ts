export const common = {
  navigateTo: (url) => {
    cy.visit(url, { timeout: 30000 })
  }
};
