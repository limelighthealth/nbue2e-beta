export const loginPage = {
  visitLoginPage: () => {
    cy.visit("/");
    cy.url().should("include", "login");
  },

  attemptToLogin: (username: string, password: string) => {
    cy.get("#loginId").type(username);
    cy.get("#loginPassword").type(password);
    return cy.get('#submitLogin').click();
  },

  checkLoginSuccess: () => {
    cy.get('#home-navigate-to-groups').should('be.visible')
  }
};
