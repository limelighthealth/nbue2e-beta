
import { When } from "cypress-cucumber-preprocessor/steps";
import { navigateToPage } from '../../service-layer/ServiceLayerNavigation';

When(`I navigate to {string} page`, async function (pageName) {
  const groupId: string = Cypress.env('groupId');
  await navigateToPage(pageName, groupId);
});