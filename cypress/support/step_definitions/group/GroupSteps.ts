import { When } from "cypress-cucumber-preprocessor/steps";
import { CreateGroupData } from '../../service-layer/dataTableClasses/CreateGroupData'
import { createGroup } from '../../service-layer/GroupServiceLayer';
import * as pathUtils from 'path';

When('I create a group via QSL with the following data', async function (dataTable) {
  const createGroupData: CreateGroupData = new CreateGroupData(dataTable);
  let filePath: string = `/group/${createGroupData.groupFileName}`;
  filePath = filePath.split(' ').join('_') + '.json';
  filePath = pathUtils.normalize('cypress/support/test_data/' + filePath);
  cy.readFile(filePath).then(async (groupJson) => {
    groupJson = createGroupData.buildPayload(groupJson);
    await createGroup(groupJson, createGroupData.ownerName);
  });
});