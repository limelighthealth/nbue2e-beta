import { Given, When, Then } from "cypress-cucumber-preprocessor/steps";
import { loginPage } from "../../pages/login-page";
import * as loginDetails from "../../../config/users.json";
import { login } from "../../service-layer/LoginServiceLayer";

Given(/I launch the FINEOS NB&U application/, async function() {
  loginPage.visitLoginPage();
});

When("I login as a {string}", async function (userRole: string) {
  cy.visit('/');
  userRole = userRole.toLocaleLowerCase().split(" ").join("_");
  const implementation: string = Cypress.env('implementation');
  const user = loginDetails[implementation][userRole];
  login(user);

  //not sure this works...
  cy.wrap(user).as('currentUser');
});

Then(
  /I should be able two access create groups, groups proposals, and tools/,
  () => {
    loginPage.checkLoginSuccess();
  }
);
