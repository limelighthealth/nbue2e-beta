import { Before } from 'cypress-cucumber-preprocessor/steps';
import { TestConfig } from '@limelight/quotepad-service/dist/spec/testConfig/TestConfig';
import { QuotepadServiceConfig } from '@limelight/quotepad-service/dist/src/QuotepadServiceConfig';
import * as environments from '../../../config/environments.json';

Before(() => {
  TestConfig.setup();
  TestConfig.cleanNodeContainerState();
})

Before({ tags: '@wip' }, function () {
  return 'pending';
});

Before({ tags: '@automation' }, async function () {
  Cypress.config("baseUrl", environments['automation'][Cypress.env('environment')]);
  Cypress.env('implementation', 'automation');
  QuotepadServiceConfig.config.setBaseUrl(Cypress.config("baseUrl"));
});

Before({ tags: '@principal' }, function () {
  Cypress.config("baseUrl", environments['principal'][Cypress.env('environment')]);
  Cypress.env('implementation', 'principal');
  QuotepadServiceConfig.config.setBaseUrl(Cypress.config("baseUrl"));
});

Before({ tags: '@transamerica' }, function () {
  Cypress.config("baseUrl", environments['transamerica'][Cypress.env('environment')]);
  Cypress.env('implementation', 'transamerica');
  QuotepadServiceConfig.config.setBaseUrl(Cypress.config("baseUrl"));
});
