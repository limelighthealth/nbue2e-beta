export { }; // needed in files which don't have an import to trigger ES6 module usage
declare global {
  namespace Cypress {
  }
}

Cypress.Commands.add("pageTitleShouldContain", (title: string) => {
  cy.get('[data-test-id ="resource-title"]')
    .should("be.visible")
    .and("contain.text", title);
});

Cypress.Commands.add("clickBreadcrumbLink", (linkName: string) => {
  cy.get('nav[aria-label="Breadcrumb"] ol li a').contains(linkName).click();
});

before(() => {
});
