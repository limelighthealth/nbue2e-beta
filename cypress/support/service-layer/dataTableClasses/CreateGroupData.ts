export class CreateGroupData {
  public groupFileName: string;
  public ownerName: string;
  public postalCode: string;
  public county: string;
  public effectiveOn: string;
  public pastEffectiveDateMonths: number;
  public sic: string;

  constructor(dataTable) {
    this.groupFileName = dataTable.hashes()[0]['group_file_name'];
    this.ownerName = dataTable.hashes()[0]['owner_name'];
    this.postalCode = dataTable.hashes()[0]['postal_code'];
    this.county = dataTable.hashes()[0]['county'];
    this.effectiveOn = dataTable.hashes()[0]['effective_on'];
    this.pastEffectiveDateMonths = Number(dataTable.hashes()[0]['past_effective_date_months']);
    this.sic = dataTable.hashes()[0]['sic'];
  }

  buildPayload(groupJSON: object): object {
    groupJSON['postal_code'] = this.postalCode ? this.postalCode : groupJSON['postal_code'];
    groupJSON['county'] = this.county ? this.county : groupJSON['county'];
    groupJSON['sic'] = this.sic ? this.sic : groupJSON['sic'];

    if (this.pastEffectiveDateMonths) {
      let date = new Date();
      date = new Date(date.setMonth(date.getMonth() - this.pastEffectiveDateMonths));
      date = new Date(date.setDate(1));
      groupJSON['default_effective_on'] = date;
    } else if(this.effectiveOn) {
      groupJSON['default_effective_on'] = this.effectiveOn;
    } else {
      let date = new Date();
      date = new Date(date.setMonth(date.getMonth() + 1));
      date = new Date(date.setDate(1));
      groupJSON['default_effective_on'] = date;
    }

    if(!groupJSON['name']){
      groupJSON['name'] = `qsl group ${new Date()}`;
    }
    return groupJSON;
  }
}
