import { Group } from '@limelight/quotepad-service/dist/src/group/domain/Group';
import { GroupServiceFactory } from '@limelight/quotepad-service/dist/src/group/factory/GroupServiceFactory';
import { GroupOwner } from '@limelight/quotepad-service/dist/src/group/domain/GroupOwner';
import { GetAvailableGroupOwnersResponse } from '@limelight/quotepad-service/dist/src/group/response/GetAvailableGroupOwnersResponse';
import { GetGroupResponse } from '@limelight/quotepad-service/dist/src/group/response/GetGroupResponse';

export async function createGroup(groupData, ownerName: string) {
  const group: Group = new Group(groupData);
  const createGroupRequest = GroupServiceFactory.createCreateGroupRequest({group: group});
  group.ownerId = await getGroupOwnerId(ownerName);
  cy.request('POST', '/v1/group', createGroupRequest.payload()).then(async (response) => {
    const groupId: string = response.body['data']['id'];
    Cypress.env('groupId', groupId);
  });
}

/**
 * returns the user Id of the specified name.
 * The ownerName MUST be in format "<firstName> <lastName>" with a space
 * between the first and last names.
 * @param ownerName
 * @returns
 */
async function getGroupOwnerId(ownerName: string): Promise<string> {
  const groupService = GroupServiceFactory.getGroupService();
  const getAvailableOwnersRequest = GroupServiceFactory.createGetAvailableGroupOwnersRequest();
  const getAvailableOwners: GetAvailableGroupOwnersResponse = await groupService.getAvailableGroupOwners(
    getAvailableOwnersRequest
  );
  const groupOwners: GroupOwner[] = getAvailableOwners.availableOwners;
  const firstName: string = ownerName.split(' ')[0];
  const lastName: string = ownerName.split(' ')[1];
  const groupOwner: GroupOwner = groupOwners.find(
    owner => owner.firstName === firstName && owner.lastName === lastName
  );
  if (groupOwner) {
    return groupOwner.id;
  }
  throw new Error(`${ownerName} not an available owner`);
}

export async function getGroup(groupId: string): Promise<GetGroupResponse> {
  try {
    let request = GroupServiceFactory.createGetGroupRequest({
      groupId: groupId
    });
    return await GroupServiceFactory.getGroupService().getGroup(request);
  } catch (error) {
    return error;
  }
}
