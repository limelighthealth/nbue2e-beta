import { QuotepadServiceConfig } from '@limelight/quotepad-service/dist/src/QuotepadServiceConfig';

/**
 * navigates to the specified page. Group ID may or may not
 * be required for some pages.
 * @param pageName 
 * @param groupId 
 */
export async function navigateToPage(pageName: string, groupId?: string) {
  const envUrl: string = QuotepadServiceConfig.config.getBaseUrl();
  cy.visit(envUrl + getUrl(pageName, groupId));
}

function getUrl(pageName: string, groupId?: string): string {
  switch (pageName) {
    case 'Home': { return getHomeUrl(); }
    case 'Group List': { return getGroupListUrl(); }
    case 'Group Home': { return getGroupHomeUrl(groupId); }
    case 'Broker List': { return getGroupBrokerListUrl(); }
    case 'New Group': { return getNewGroupProfileUrl(); }
    case 'Group Profile': { return getGroupProfileUrl(groupId); }
    case 'Import Census': { return getImortCensusUrl(groupId); }
    case 'Edit Census': { return getEditCensusUrl(groupId); }
    case 'Edit Census Mode Edit': { return getEditCensusModeEditUrl(groupId); }
    case 'Census v2': { return getCensusV2(groupId); }
    case 'Plan Designer': { return getPlanDesignerUrl(groupId); }
    case 'Select Plans': { return getSelectPlansUrl(groupId); }
    case 'Quotes': { return getQuotesUrl(groupId); }
    case 'Classes': { return getClassesUrl(groupId); }
    case 'Proposals': { return getProposalsUrl(groupId); }
    case 'Documents': { return getDocumentsUrl(groupId); }
    case 'Underwriting': { return getUnderwritingUrl(groupId); }
    default: {
      throw new Error(`Cannot find URL for page ${pageName}`);
    }
  }
}

function getHomeUrl(): string {
  return '/home';
}

function getGroupListUrl(): string {
  return '/groups';
}

function getGroupHomeUrl(groupId: string): string {
  return `/groups/${groupId}`;
}

function getGroupBrokerListUrl(): string {
  return '/groups/brokerList';
}

function getNewGroupProfileUrl(): string {
  return '/groups/new';
}

function getGroupProfileUrl(groupId: string): string {
  return `/groups/${groupId}/edit`;
}

function getImortCensusUrl(groupId: string): string {
  return `/groups/${groupId}/import?overwrite=true&overwrite_classes=true`;
}

function getEditCensusUrl(groupId: string): string {
  return `/groups/${groupId}/editCensus`;
}

function getEditCensusModeEditUrl(groupId: string): string {
  return `/groups/${groupId}/editCensus?mode=edit`;
}

function getCensusV2(groupId: string): string {
  return `/groups/${groupId}/census`;
}

function getPlanDesignerUrl(groupId: string): string {
  return `/tools/planDesigner/${groupId}`;
}

function getSelectPlansUrl(groupId: string): string {
  return `/tools/selectPlan/${groupId}?is_current=false&is_renewal=false`;
}

function getQuotesUrl(groupId: string): string {
  return `/groups/${groupId}/quotes`;
}

function getClassesUrl(groupId: string): string {
  return `/groups/${groupId}/classes`;
}

function getProposalsUrl(groupId: string): string {
  return `/proposals?group_id=${groupId}`;
}

function getDocumentsUrl(groupId: string): string {
  return `/groups/${groupId}/documents?mode=create`;
}

function getUnderwritingUrl(groupId: string): string {
  return `/groups/${groupId}/underwriting`;
}