import { TestConfig } from '@limelight/quotepad-service/dist/spec/testConfig/TestConfig';
import { TestSession } from '@limelight/quotepad-service/dist/spec/testConfig/TestSession';
import { AuthenticationServiceFactory } from '@limelight/quotepad-service/dist/src/authentication/factory/AuthenticationServiceFactory';

export function login(loginRequest) {
  try {
    const authRequest = AuthenticationServiceFactory.createAuthenticateRequest(loginRequest['username'], loginRequest['password']);
    cy.request('POST', '/api/user/login', authRequest.payload()).then(async (response) => {
      if (response.status === 200) {
        if (response.headers['set-cookie'] && response.headers['set-cookie'].length !== 1) {
          response.headers['set-cookie'] = getArrowCookie(response.headers['set-cookie']);
          await TestConfig.setupForNewLogin(response);
          await setCookies();
        }
      }
    })
  } catch (error) {
    throw new Error("Authentication Failure: " + error);
  }
}

export async function setCookies() {
  const cookies = await TestSession.getInstance().getCookies();
  const cookieName: string = cookies.split('=')[0];
  const cookieValue: string = cookies.split('=')[1];
  cy.setCookie(cookieName, cookieValue);
}

/**
 * If multiple cookies are returned in the header, just return the Arrow
 * cookie.
 * @param cookies 
 */
function getArrowCookie(cookies: string | Array<string>): Array<string> {
  for (let cookie of cookies) {
    if (cookie.indexOf('Arrow=') !== -1) {
      return new Array(cookie);
    }
  }
  throw new Error('Arrow cookie not defined');
}