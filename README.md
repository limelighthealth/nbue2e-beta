# cypress-cucumber-typescript
Example of using Cypress with Cucumber and TypeScript

All the configuration is in [cypress/plugins/index.js](cypress/plugins/index.js)

## POC using page object Models
TypeScript step definitions are in [cypress/support/step-definitions](cypress/support/step-definitions)

(POC using cypress-cucumber-preprocessor [package.json](package.json) )

### command line to run tests passing environment variables

* use **tags** to run the desired implementation
* use **environment** after -e flag to run the desired environment (In the same way you can pass any environment variable)

**Example**
npx cypress-tags run **tags**=@automation -e **environment**=master
