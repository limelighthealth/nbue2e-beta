FROM cypress/base:12

WORKDIR /app

COPY package.json .
RUN npm install

COPY ./cypress ./cypress
COPY ./cypress.json .
